package com.geektechnique.hypermedia.repository;

import com.geektechnique.hypermedia.domain.Author;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface AuthorRepository extends PagingAndSortingRepository<Author, Long> {

}
