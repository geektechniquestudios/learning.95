package com.geektechnique.hypermedia.repository;

import java.util.List;

import com.geektechnique.hypermedia.domain.Post;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface PostRepository extends PagingAndSortingRepository<Post, Long> {

		List<Post> findByTitleContaining(@Param("title") String title);
	
}
